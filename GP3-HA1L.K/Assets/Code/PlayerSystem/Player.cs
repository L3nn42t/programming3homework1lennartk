﻿using System;
using UEGP3.Core;
using UEGP3.InventorySystem;
using UnityEngine;
using UnityEngine.UI;

namespace UEGP3.PlayerSystem
{
	/// <summary>
	/// Player containing additional logic like item pick ups etc.
	/// </summary>
	public class Player : MonoBehaviour
	{
		[Tooltip("Inventory to be used for the player")] [SerializeField] 
		private Inventory _playerInventory;

		//HA-Code
		[SerializeField]
		private GameObject _inventoryUI;
		[SerializeField]
		private Text _inventoryText;

		// TODO 
		// This code could potentially be simplified, if it was in an array, like so: Text[] _bagUIs;
		[SerializeField]
		private Text _bag1UI;
		[SerializeField]
		private Text _bag2UI;
		[SerializeField]
		private Text _bag3UI;

		
		private void Start()
		{
			_inventoryUI.SetActive(false);
			
		}
		private void Update()
		{
			// Show Inventory if button is pressed
			if (Input.GetButtonDown("Inventory"))
			{
				_playerInventory.ShowInventory();
				
				
				if (_inventoryUI.activeSelf == false)
				{
					OpenInventoryUI();
					//_playerInventory.MoveItemtoBag();
				}
				else
				{
					CloseInventoryUI();
				}

			}

			if (Input.GetButtonDown("ItemQuickAccess"))
			{
				_playerInventory.UseQuickAccessItem();
			}

			
		}

		private void OnTriggerEnter(Collider other)
		{
			// If we collide with a collectible item, collect it
			ICollectible collectible = other.gameObject.GetComponent<ICollectible>();
			if (collectible != null)
			{
				Collect(collectible);
			}
		}

		private void Collect(ICollectible collectible)
		{
			collectible.Collect(_playerInventory);
		}
		
		private void OpenInventoryUI()
		{
			//HA-Code
			Cursor.visible = true; // to use the Slider for Player controller
			_inventoryUI.SetActive(true);
			string inventory = _playerInventory.ToString();
			// TODO
			// here, arrays would be helpful, like so: string[] bagContents = new string[_playerInventory.Bags.Length];
			// for (int i = 0; i < _playerINventory.Bags.Length; i++) { bagContents[i] = _playerInventory.Bags[i].ToString(); }
			string bag1 = _playerInventory._bag1.ToString();
			string bag2 = _playerInventory._bag2.ToString();
			string bag3 = _playerInventory._bag3.ToString();
			_inventoryText.text = inventory;
			// TODO
			// Above i said these could be in an array. If we have it in an array, we can now do something like this:
			// for(int i = 0; i < _bagsUI.Length;i++) { _bagUIs[i].text = bagContent[i] }
			_bag1UI.text = bag1;
			_bag2UI.text = bag2;
			_bag3UI.text = bag3;
		}
		private void CloseInventoryUI()
		{
			_inventoryUI.SetActive(false);
			Cursor.visible = false;
		}
	}
}