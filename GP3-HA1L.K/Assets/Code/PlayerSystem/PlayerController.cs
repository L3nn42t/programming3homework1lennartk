﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UEGP3.PlayerSystem
{
	[RequireComponent(typeof(CharacterController))]
	public class PlayerController : MonoBehaviour
	{
		[Header("General Settings")] [Tooltip("The speed with which the player moves forward")] [SerializeField]
		private float _movementSpeed = 10f;
		[Tooltip("The graphical represenation of the character. It is used for things like rotation")] [SerializeField]
		private Transform _graphicsObject = null;
		[Tooltip("Reference to the game camera")] [SerializeField]
		private Transform _cameraTransform = null;

		[Header("Movement")] [Tooltip("Smoothing time for turns")] [SerializeField]
		private float _turnSmoothTime = 0.15f;
		[Tooltip("Smoothing time to reach target speed")] [SerializeField]
		private float _speedSmoothTime = 0.7f;
		[Tooltip("Modifier that manipulates the gravity set in Unitys Physics settings")] [SerializeField]
		private float _gravityModifier = 1.0f;
		[Tooltip("Maximum falling velocity the player can reach")] [Range(1f, 15f)] [SerializeField]
		private float _terminalVelocity = 10f;
		[Tooltip("The height in meters the cahracter can jump")] [SerializeField]
		private float _jumpHeight;
		
		[Header("Ground Check")] [Tooltip("A transform used to detect the ground")] [SerializeField]
		private Transform _groundCheckTransform = null;
		[Tooltip("The radius around transform which is used to detect the ground")] [SerializeField]
		private float _groundCheckRadius = 0.1f;
		[Tooltip("A layermask used to exclude/include certain layers from the \"ground\"")] [SerializeField]
		private LayerMask _groundCheckLayerMask = default;

		// Use formula: Mathf.Sqrt(h * (-2) * g)
		private float JumpVelocity => Mathf.Sqrt(_jumpHeight * -2 * Physics.gravity.y);
		
		private bool _isGrounded;
		private float _currentVerticalVelocity;
		private float _currentForwardVelocity;
		private float _speedSmoothVelocity;
		private CharacterController _characterController;

		//HA Code
		[SerializeField]
		private float _dashDistance;
		 
		// TODO 
		// why is this unused? It would've been a great implementation! 
		private float DashVelocity => Mathf.Sqrt(_dashDistance * -1 * Physics.gravity.x);

		// TODO 
		// Sorry for the slight misunderstanding - I didn't mean a "UI Slider" but an inspector slider. 
		// You can create a slider in the inspector with the [Range]-Attribute, see _terminalVelocity field.
		//AirControl
		[SerializeField]
		private Slider airslider;
		[SerializeField]
		private float _airFloat;
		[SerializeField]
		private bool _airControl = true;

		// TODO 
		// Unused variables, clean them up if not required
		private float _tempx;
		private float _tempy;
		private float _tempz;
		//HA-Code end

		private void Awake()
		{
			_characterController = GetComponent<CharacterController>();
		}

		private void Update()
		{
			_airFloat = airslider.value;
			if (_airFloat >= 0.5)
			{
				_airControl = true;
			}
			else
			{
				_airControl = false;
				
			}

			// Fetch inputs
			// GetAxisRaw : -1, +1 (0) 
			// GetAxis: [-1, +1]
			
			float horizontalInput = Input.GetAxisRaw("Horizontal");
			float verticalInput = Input.GetAxisRaw("Vertical");
			bool jumpDown = Input.GetButtonDown("Jump");

			bool dashInput = Input.GetButtonDown("Dash");

			// Calculate a direction from input data 
			Vector3 direction = new Vector3(horizontalInput, 0, verticalInput).normalized;

			if (!_isGrounded)
			{
				// TODO 
				// Interesting! I didn't expect you to do it like this. I rather thought of the air control as a smooth value that goes from 0-1
				// And also smoothly works as such (0.5 then would be only "half" the control while airborne). The way I achieved this effect was by
				// directly manipulating the smooth time. I added a method below, you can fiddle around with it. 
				// 
				if(_airControl == false) // problem is lack of gravity, maybe a coroutine that switches aircontril after  a second for for a second?
				{
					direction = Vector3.zero;  //both are possible, but both have a tendency to get stuck on Surfaces
					//_turnSmoothTime = 1f; // note: i got the idea while discussing how to do this with a college, //suddenly didnt work?
				}			
			}
			

			// If the player has given any input, adjust the character rotation
			if (direction != Vector3.zero)
			{
				float lookRotationAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + _cameraTransform.eulerAngles.y;
				Quaternion targetRotation = Quaternion.Euler(0, lookRotationAngle, 0);
				// TODO
				// With my solution, we would call the method given below here like so: GetSmoothTimeAfterAirControl(_turnSmoothTime, false)
				// the function takes a bool because for some smoothing functions a high value is required for 0 control, while others require a very low one for that.
				_graphicsObject.rotation = Quaternion.Slerp(_graphicsObject.rotation, targetRotation, _turnSmoothTime);
			}

			// Calculate velocity based on gravity formula: delta-y = 1/2 * g * t^2
			// We ignore the 1/2 to safe multiplications and because it feels better.
			// Second Time.deltaTime is done in controller.Move()-call so we save one multiplication here.
			_currentVerticalVelocity += Physics.gravity.y * _gravityModifier * Time.deltaTime;
			
			// Clamp velocity to reach no more than our defined terminal velocity
			_currentVerticalVelocity = Mathf.Clamp(_currentVerticalVelocity, -_terminalVelocity, JumpVelocity);

			// Calculate velocity vector based on gravity and speed
			// (0, 0, z) -> (0, y, z)
			float targetSpeed = (_movementSpeed * direction.magnitude);
			_currentForwardVelocity = Mathf.SmoothDamp(_currentForwardVelocity, targetSpeed, ref _speedSmoothVelocity, _speedSmoothTime);
			Vector3 velocity = _graphicsObject.forward * _currentForwardVelocity + Vector3.up * _currentVerticalVelocity;


			// TODO 
			// The implementation like this works. However you created a property DashVelocity.
			// Did you intend to use that instead here? What is the reason you did not use it?
			//DashCode
			if (dashInput)
			{
				_currentForwardVelocity += _dashDistance;
			}

			
			


			// Use the direction to move the character controller
			// direction.x * Time.deltaTime, direction.y * Time.deltaTime, ... -> resultingDirection.x * _movementSpeed
			// Time.deltaTime * _movementSpeed = res, res * direction.x, res * direction.y, ...
			_characterController.Move(velocity * Time.deltaTime);
			
			// Check if we are grounded, if so reset gravity
			_isGrounded = Physics.CheckSphere(_groundCheckTransform.position, _groundCheckRadius, _groundCheckLayerMask);
			if (_isGrounded)
			{
				// Reset current vertical velocity
				_currentVerticalVelocity = 0f;
			}

			// If we are grounded and jump was pressed, jump
			if (_isGrounded && jumpDown)
			{
				_currentVerticalVelocity = JumpVelocity;
				
			}
		}
		// TODO 
		// please remove useless code - this will make correcting easier for me and it will make your code more readable
		// in longer projects, this is crucial. If you are concerned I don't see what you worked on: commit often! I will see
		// how you got to your solution and what happened on the road. If everythign works fine, I wont check your git history.
		// If there is something wrong, I will check it out and see how you got to your code, so I can award the effort you made 
		// even though something doesn't work 100%. So, if you have code that you used for some trial and error, you can safely remove 
		// it once it gets unused. Should you ever need it again, you can recover it from your git log anyways!
		private void OnCollisionStay(Collision other) // Useless Code,
		{
			StartCoroutine(AirControlOn());
		}
		
		private IEnumerator AirControlOn()
		{
			if(_airControl == false)
			{
				_airControl = true;
				yield return new WaitForSeconds(1f);
				_airControl = false;
			   
			}
			
		}

		// TODO 
		// Function for Air Control given by Kevin
		private float _airControlFloat;
		/// <summary>
		/// Calculates the smoothTime based on airControl.
		/// </summary>
		/// <param name="smoothTime">The initial smoothTIme</param>
		/// <param name="zeroControlIsMaxValue">If we do not have air control, is the smooth time float.MaxValue or float.MinValue?</param>
		/// <returns>The smoothTime after regarding air control</returns>
		private float GetSmoothTimeAfterAirControl(float smoothTime, bool zeroControlIsMaxValue)
		{
			// We are grounded, don't modify smoothTime
			if (_characterController.isGrounded)
			{
				return smoothTime;
			}

			// Avoid divide by 0 exception
			if (Math.Abs(_airControlFloat) < Mathf.Epsilon)
			{
				return zeroControlIsMaxValue ? float.MaxValue : float.MinValue;
			}

			// smoothTime is influenced by air control
			return smoothTime / _airControlFloat;
		}
	}
}
