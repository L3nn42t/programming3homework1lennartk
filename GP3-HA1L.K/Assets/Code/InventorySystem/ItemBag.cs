﻿using System.Collections;
using System.Collections.Generic;
using UEGP3.InventorySystem;
using UnityEngine;

// TODO
// Add some comments to your code, especially if it is a completely new class.
// Also try to put each class into an appropriate namespace
[CreateAssetMenu(menuName = "UEGP3/Inventory System/New ItemBag", fileName = "New ItemBag")]
public class ItemBag : ScriptableObject
{
    [SerializeField]
    public Item _itemtype;

    [SerializeField]
    public int _bagSize = 4;

    public Dictionary<Item , int> _itemBag = new Dictionary<Item, int>();

    
	public override string ToString()
	{

		// "String-Interpolation": $ before a string "" allows us to use variables in {} 
		// inventory = "Inventory " + name + " contains:\r\n" is the same as the line below, but nicer! :) 
		string inventory = $"The {name} contains:\r\n";


		foreach (KeyValuePair<Item, int> inventoryItem in _itemBag)
		{
			inventory += $"[{inventoryItem.Key.ItemName} - {inventoryItem.Value}]\r\n";

		}


		return inventory;
	}

}
