﻿using System;
using UEGP3.Core;
using UnityEngine;

namespace UEGP3.InventorySystem
{
	[RequireComponent(typeof(SphereCollider))]
	public class ItemPickup : MonoBehaviour, ICollectible
	{
		[Tooltip("The scriptable object of the item to be picked up.")] [SerializeField]
		private Item _itemToPickup;
		[Tooltip("Range in which the pick up is performed.")] [SerializeField] 
		private float _pickupRange;
		[Tooltip("SFX that is being played when items are being picked up")] [SerializeField] 
		private ScriptableAudioEvent _pickupSFX;

		private AudioSource _audioSource;
		private SphereCollider _pickupCollider;

		[SerializeField]
		private Mesh _itemMesh;

		private void Awake()
		{
			Mesh mesh = GetComponentInChildren<MeshFilter>().mesh;
			mesh = Instantiate(_itemMesh);// mesh = _itemMesh doesn't work for some reason
			// TODO you missed to properly re-assign the value:
			GetComponentInChildren<MeshFilter>().mesh = mesh;
			// A better solution would be to relocate this field and move it to the Item class
			// The advantage is, that the same items probably always have the same meshes. This way,
			// we can then set the mesh property only on the ScriptableObject assets. If we assign a coin
			// mesh to a coin pickup, each coin pickup will have it then, although we referenced it in only one place!
			// Otherwise, you'd have to set the coin mesh in each pickup thats supposed to be a coin, as well as have to 
			// swap the mesh, once you swap the Item ScriptableObject on a pick up. That would introduce a lot error potential.
			// Does that make sense? 
			// Then we can assign the mesh like so: 
			// GetComponentInChildren<MeshFilter>().mesh = _itemToPickup.ItemMesh;

			// Get collider on same object
			_pickupCollider = GetComponent<SphereCollider>();
			_audioSource = FindObjectOfType<AudioSource>();
			
			// Ensure collider values are set accordingly
			_pickupCollider.radius = _pickupRange;
			_pickupCollider.isTrigger = true;
		}

		public void Start()
		{
			// change Mesh to a predefined one
			

			
		}
		public void Collect(Inventory inventory)
		{
			// Add item to inventory
			bool wasPickedUp = inventory.TryAddItem(_itemToPickup);

			// Destroy the pickup once the object has been successfully picked up
			if (wasPickedUp)
			{
				// Play the pickup SFX when picking up the item
				_pickupSFX.Play(_audioSource);
				Destroy(gameObject);
			}
		}
		
#if UNITY_EDITOR
		private void OnValidate()
		{
			if (!_pickupCollider)
			{
				_pickupCollider = GetComponent<SphereCollider>();
			}
			
			// Ensure radius is set correctly
			_pickupCollider.radius = _pickupRange;
		}
#endif
	}
}